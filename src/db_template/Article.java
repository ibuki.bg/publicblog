package db_template;

public class Article {
	int articleId;
	String date;
	int userId;
	String title;
	String contents;
	String location;
	String deleteTime;

	public Article(int articleId, String date, int userId, String title, String contents, String location,
			String deleteTime) {
		this.articleId = articleId;
		this.date = date;
		this.userId = userId;
		this.title = title;
		this.contents = contents;
		this.location = location;
		this.deleteTime = deleteTime;
	}

	public Article(int articleId, String title, String contents) {
		this.articleId = articleId;
		this.title = title;
		this.contents = contents;
	}

}
