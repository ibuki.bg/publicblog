package db_template;

import java.util.List;

import org.junit.jupiter.api.Test;

class DaoTest {

	static final String dbName = "blogdb";
	static final String user = "root", pass = "admin";

	void addTest() {

		Dao test = new Dao(dbName, user, pass);

		String[] strs = { "articleID", "title", "contents" };

		List<List<String>> result = test.selectSql("select articleID,contents,title from article", strs);

		for (List<String> row : result) {
			for (String content : row) {
				System.out.println(content);
			}
		}
	}

	void insertTest() {
		Dao test = new Dao(dbName, user, pass);
		String sql = "Insert into user (mailadress, passWord) values(?, ?)";
		String[] values = { "gg@gg", "ggggggggg" };

		int returnValue = test.upSertSql(sql, values);

		System.out.println(returnValue);
	}

	void updateTest() {
		Dao test = new Dao(dbName, user, pass);
		String sql = "UPDATE user SET mailAdress = ?,passWord = ? WHERE (userID = ?);";
		String[] values = { "g@g", "hhhhhhhh", "7" };

		int returnValue = test.upSertSql(sql, values);

		System.out.println(returnValue);
	}

	void insertDaoTest() {
		Dao test = new Dao(dbName, user, pass);
		test.createUser("h@h", "oooooo");
	}

	void deleteDaoTest() {
		Dao test = new Dao(dbName, user, pass);
		test.deleteUser(8);
	}

	@Test
	void selectArticleTest() {
		Dao test = new Dao(dbName, user, pass);
		List<Article> articles = test.getArticle();
		for (Article article : articles) {
			System.out.println(article.articleId);
			System.out.println(article.title);
			System.out.println(article.contents);
		}
	}

}
